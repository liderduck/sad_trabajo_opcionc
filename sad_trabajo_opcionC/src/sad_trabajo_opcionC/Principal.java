package sad_trabajo_opcionC;





import weka.classifiers.Evaluation;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.MultiFilter;
import weka.filters.unsupervised.attribute.InterquartileRange;
import weka.classifiers.functions.SMO;
import weka.classifiers.functions.supportVector.NormalizedPolyKernel;
import weka.classifiers.meta.FilteredClassifier;


	public class Principal {
		
	    public static void main(String[] args) throws Exception {
	    	Lectura lect= new Lectura();	    	
			Result resultados = new Result();
			System.out.println("Empezamos con la manipulacion de los datos");
	    	
			/////////////Lectura de datos /////////////
	    	Instances dataTrain;
	    	dataTrain = lect.cargarDatos(args[0]);
	    	
	    	Instances dataTest;
	    	dataTest = lect.cargarDatos(args[1]);
	    	
	    	//creamos el filtro SMO 
	    	SMO smo = new SMO();
	    	NormalizedPolyKernel  npk = new NormalizedPolyKernel();
	    	smo.setKernel(npk);
	    	smo.setC(2.0);
	    	
	    	
	    	//cramos el multifilter
	    	MultiFilter multiFilter = new MultiFilter();
	    	Filter[] filter= new Filter[1];//le decimos que va a tener una tama�o de dos puesto que seran 2 filtros
	    	  	
	    	//creamos el interquartileRange
	    	InterquartileRange itqr = new InterquartileRange();
	    	itqr.setDetectionPerAttribute(true);
	    	itqr.setExtremeValuesFactor(10.0);
	    	itqr.setOutlierFactor(1.0);
	    	filter[0]= itqr;
	    	
	    	
	    	multiFilter.setFilters(filter);
	    	
	    	//creamos el clasificador
	    	FilteredClassifier clasificador = new FilteredClassifier();
	    	clasificador.setClassifier(smo);
	    	clasificador.setFilter(multiFilter);
	    	
	    	//preparamos el train vs test
			clasificador.buildClassifier(dataTrain);
			Evaluation evaluator = new Evaluation(dataTrain);
			evaluator.evaluateModel(clasificador, dataTest);
			
			//imprimimos los resultados
			resultados.imprimirResultados(evaluator);
			
	    }
}


